import GUID from './GUID';

// 矩阵行标题
export default class MatrixRowTitle {
    public Index: number = 0;
    public TitleId!: string;
    public Title: string = '';
    // 多选值
    public CheckSelValue: string[] = [];
    //填空值
    public CheckBlack:string[]=[];
    // 单选值
    public RadioSelValue: string = '';
    

    constructor(index: number, title: string) {
        this.Title = title;
        this.TitleId = new GUID().toString();
        this.Index = index;
    }

}
