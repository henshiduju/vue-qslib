import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';

// 比重分配项
export class ProportionOption extends BaseOption {
    // 比重值
   public ItemValue: number = 0;

}
export default class ProportionSubject extends BaseQuestionSubject<ProportionOption> {
    public static option: ProportionOption;
    // 比重单位
    public Unit: string = '%';
    // 分配的总值
    public TotalValue: number = 100;



}
